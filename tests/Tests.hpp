#ifndef TWTPP_TESTS_HPP
#define TWTPP_TESTS_HPP

#include "../src/extern/catch.hpp"

#include "../src/lib/Twtpp.hpp"

Twtpp PrepareTest() {
    char* bearerToken = std::getenv("bearertoken");
    char* apiKey = std::getenv("apikey");
    char* apiSecret = std::getenv("apisecret");

    if (bearerToken == nullptr) {
        std::cout << "Bearer token undefined!" << std::endl;
    }
    if (apiKey == nullptr) {
        std::cout << "Api key undefined!" << std::endl;
    }
    if (apiSecret == nullptr) {
        std::cout << "Api secret undefined!" << std::endl;
    }

    return Twtpp(bearerToken == nullptr ? "" : bearerToken, apiKey == nullptr ? "" : apiKey, apiSecret == nullptr ? "" : apiSecret);
}

#endif //TWTPP_TESTS_HPP
