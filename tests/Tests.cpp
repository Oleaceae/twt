#include "Tests.hpp"

TEST_CASE("search/recent", "[search]") {
    auto test = PrepareTest();

    auto results = test.Search_Recent_Tweets("from:WhiteHouse");
    auto resultStr = results.raw();
    auto resultJson = results.json();

    CHECK(!resultStr.empty());
    CHECK(resultJson["data"].is_array());
}

TEST_CASE("search/all", "[.][search][academic]") {
    auto test = PrepareTest();

    auto results = test.Search_All_Tweets("from:WhiteHouse");
    auto resultStr = results.raw();
    auto resultJson = results.json();

    CHECK(!resultStr.empty());
    CHECK(resultJson["data"].is_array());
}

TEST_CASE("counts/recent", "[counts]") {
    auto test = PrepareTest();

    auto results = test.Tweets_Count_Recent("from:WhiteHouse");
    auto resultJson = results.json();

    CHECK_NOTHROW(resultJson["meta"]["total_tweet_count"].get<int>());
}