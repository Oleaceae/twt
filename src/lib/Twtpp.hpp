#ifndef TWTPP_TWTPP_HPP
#define TWTPP_TWTPP_HPP

#define CPPHTTPLIB_OPENSSL_SUPPORT

#include "../extern/httplib.hpp"
#include "../extern/json.hpp"

typedef std::string String;
typedef long long llong;

class Twtpp {
private:
    String BearerToken{};
    String APIKey{};
    String APIKeySecret{};

    class ResultContainer {
    private:
        String Result;

    public:
        explicit ResultContainer(String& result) : Result(result) {};

        String raw() {
            return Result;
        }

        nlohmann::json json() {
            return nlohmann::json::parse(Result);
        }
    };

    ResultContainer Tweet(String Endpoint, const String &Query) {
        httplib::Client twt("https://api.twitter.com");

        httplib::Headers headers{
                {"Authorization", "Bearer " + BearerToken}
        };
        String path = "/2/tweets/" + Endpoint + "?query=" + Query;

        auto response = twt.Get(path, headers);

        return Twtpp::ResultContainer(response->body);
    }

public:
    Twtpp() = default;

    explicit Twtpp(String BearerToken, String APIKey = "", String APIKeySecret = "") : BearerToken{BearerToken}, APIKey{APIKey}, APIKeySecret{APIKeySecret} {}

    void SetBearerToken(String Token) {
        this->BearerToken = Token;
    }

    void SetAPIKey(String Key) {
        this->APIKey = Key;
    }

    void SetAPIKeySecret(String KeySecret) {
        this->APIKeySecret = KeySecret;
    }

    ResultContainer Search_Recent_Tweets(const String& Query) {
        return Twtpp::Tweet("search/recent", Query);
    }
    ResultContainer Search_All_Tweets(const String& Query) {
        return Twtpp::Tweet("search/all", Query);
    }
    ResultContainer Tweets_Count_Recent(const String& Query) {
        return Twtpp::Tweet("counts/recent", Query);
    }
    ResultContainer Tweets_Count_All(const String& Query) {
        return Twtpp::Tweet("counts/all", Query);
    }
};

#endif //TWTPP_TWTPP_HPP
